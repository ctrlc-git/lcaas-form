export { default as LcForm } from './render.vue';
export { default as LcFormItem } from './form-item/index.ts';
export { parseRules } from './utils/utils.ts';
export { useRegister } from './utils/registerHook.ts';
