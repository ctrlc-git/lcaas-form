import type { PluginOption } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';

/**
 * Plugin to minimize and use ejs template syntax in index.html.
 * https://github.com/anncwb/vite-plugin-html
 */
export function ConfigHtmlPlugin(env: ViteEnv, isBuild: boolean) {
  const { VITE_APP_TITLE } = env;

  const htmlPlugin: PluginOption[] = createHtmlPlugin({
    minify: isBuild,
    inject: {
      data: {
        title: VITE_APP_TITLE,
        isBuild: isBuild,
        tongji: `<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?88a1b4ba0bb227c08e62fa1f23a96f63";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
`,
      },
    },
  });
  return htmlPlugin;
}
